from collections import deque
def BFS2(G, node1, node2):
    pred = {}
    n1 = node1
    Q = deque([n1])
    marked = {n1 : True}
    for node in G.adj:
        if node != n1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if not marked[node]:
                Q.append(node)
                marked[node] = True
                # fix off by 1 error*
                pred[node] = current_node
    curr = node2
    to_find = node1
    L = []
    count = 0

    while curr != to_find and count < len(pred):
        if pred.get(curr) == None:
            return []
        L.append(curr)
        curr = pred[curr]
        count += 1
    if len(L) != 0:
        L.append(node1)
    else:
        return []

    L.reverse()
    return L

def DFS2(G, node1, node2):
    pred = {}
    S = [(node1, -1)]
    marked = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        t = S.pop()
        current_node = t[0]
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                S.append((node, current_node))
            # if not the starting node, add predecessor entry
            if t[1] != -1:
                pred[current_node] = t[1]
    curr = node2
    to_find = node1
    L = []
    count = 0
    while curr != to_find and count < len(pred):
        if pred.get(curr) == None:
            return []
        L.append(curr)
        curr = pred[curr]
        count += 1
    if len(L) != 0:
        L.append(node1)
    else:
        return []

    L.reverse()
    return L