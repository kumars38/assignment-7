from collections import deque

#Undirected graph using an adjacency list
class Graph:

    def __init__(self, n):
        self.adj = {}
        for i in range(n):
            self.adj[i] = []

    def are_connected(self, node1, node2):
        return node2 in self.adj[node1]

    def adjacent_nodes(self, node):
        return self.adj[node]

    def add_node(self):
        self.adj[len(self.adj)] = []

    def add_edge(self, node1, node2):
        if node1 not in self.adj[node2]:
            self.adj[node1].append(node2)
            self.adj[node2].append(node1)

    def number_of_nodes():
        return len()


#Breadth First Search
def BFS(G, node1, node2):
    Q = deque([node1])
    marked = {node1 : True}
    for node in G.adj:
        if node != node1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if node == node2:
                return True
            if not marked[node]:
                Q.append(node)
                marked[node] = True
    return False

#Depth First Search
def DFS(G, node1, node2):
    S = [node1]
    marked = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        current_node = S.pop()
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                if node == node2:
                    return True
                S.append(node)
    return False

#Breadth First Search 3
def BFS3(G, node1):
    pred = {}
    n1 = node1
    Q = deque([n1])
    marked = {n1 : True}
    for node in G.adj:
        if node != n1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if not marked[node]:
                Q.append(node)
                marked[node] = True
                # fix off by 1 error*
                pred[node] = current_node
    return pred

#Depth First Search 3
def DFS3(G, node1):
    pred = {}
    S = [(node1, -1)]
    marked = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        t = S.pop()
        current_node = t[0]
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                S.append((node, current_node))
            # if not the starting node, add predecessor entry
            if t[1] != -1:
                pred[current_node] = t[1]
    return pred

g = Graph(7)
g.add_edge(1,2)
g.add_edge(1,3)
g.add_edge(2,4)
g.add_edge(3,4)
g.add_edge(3,5)
g.add_edge(4,5)
g.add_edge(4,6)

'''g = Graph(6)
g.add_edge(0,1)
g.add_edge(0,2)
g.add_edge(1,3)
g.add_edge(2,3)
g.add_edge(2,4)
g.add_edge(3,4)
g.add_edge(3,5)'''

#b = BFS3(g, 1)
d = DFS3(g, 1)

print(d)