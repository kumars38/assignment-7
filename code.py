from lab7 import *
from graphs import *
import random
from xlsxwriter import Workbook
import timeit

def create_random_graph(n,m):
    edge_list = []
    G = Graph(n)
    for i in range(n):
        for j in range(i):
            edge_list.append((i,j))
    for _ in range(m):
        edge = random.choice(edge_list)
        G.add_edge(edge[0], edge[1])
        edge_list.remove(edge)
    return G

# Testing Function for Cycles
def testCycles():
    workbook = Workbook('cycles.xlsx')
    worksheet = workbook.add_worksheet()
    portion_graphs = []
    
    for j in range(0,101):
        g = []
        for i in range(100):
            g.append(create_random_graph(100,j))
        cycles = 0
        for z in range(100):
            if has_cycle(g[z]):
                cycles +=1
    
        portion_graphs.append(cycles)
        
    no = 0
    for i in range(100):
        worksheet.write(i, 0, no)
        worksheet.write(i, 1, portion_graphs[i])
        no += 1
    workbook.close() 

def testConnected():
    workbook = Workbook('connected.xlsx')
    worksheet = workbook.add_worksheet()
    portion_graphs = []
    
    for j in range(0,305,5):
        g=[]
        for i in range(100):
            g.append(create_random_graph(100,j))
        connected = 0
        for z in range(100):
            if is_connected(g[z]):
                connected +=1
        portion_graphs.append(connected)
        
    no = 0
    for i in range(60):
        worksheet.write(i, 0, no)
        worksheet.write(i, 1, portion_graphs[i])
        no += 5
    workbook.close() 



#testConnected()
testCycles()